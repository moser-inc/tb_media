class AddProtectedToSpudMedia < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_media, :attachment_updated_at, :datetime
  end
end
