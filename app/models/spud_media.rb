class SpudMedia < ActiveRecord::Base

  has_attached_file :attachment,
    storage: Spud::Media.paperclip_storage,
    s3_credentials: Spud::Media.s3_credentials,
    s3_permissions: 'public-read',
    path: Spud::Media.storage_path,
    url: Spud::Media.storage_url,
    styles: ->(attachment) { attachment.instance.dynamic_styles }

  validates :crop_x, :crop_y, :crop_w, :crop_h, :crop_s, numericality: { allow_nil: true }

  extend ActionView::Helpers::NumberHelper

  validates_attachment :attachment,
    presence: true,
    content_type: { content_type: Spud::Media::CONTENT_TYPES },
    size: {
      less_than: Spud::Media.max_upload_size,
      message: 'size cannot exceed ' + number_to_human_size(Spud::Media.max_upload_size),
      if: proc { |_p| Spud::Media.max_upload_size.positive? }
    }

  before_create :rename_file

  def rename_file
    # Remove periods and other unsafe characters from file name to make routing easier
    extension = File.extname(attachment_file_name)
    filename = attachment_file_name.chomp(extension).parameterize
    attachment.instance_write :file_name, filename + extension
  end

  def image_from_type
    if is_image?
      return attachment_url(:small)

    elsif attachment_content_type.blank?
      return 'admin/files_thumbs/dat_thumb.png'

    elsif attachment_content_type =~ /jpeg|jpg/
      return 'admin/files_thumbs/jpg_thumb.png'

    elsif attachment_content_type =~ /png/
      return 'admin/files_thumbs/png_thumb.png'

    elsif attachment_content_type =~ /zip|tar|tar\.gz|gz/
      return 'admin/files_thumbs/zip_thumb.png'

    elsif attachment_content_type =~ /xls|xlsx/
      return 'admin/files_thumbs/xls_thumb.png'

    elsif attachment_content_type =~ /doc|docx/
      return 'admin/files_thumbs/doc_thumb.png'

    elsif attachment_content_type =~ /ppt|pptx/
      return 'admin/files_thumbs/ppt_thumb.png'

    elsif attachment_content_type =~ /txt|text/
      return 'admin/files_thumbs/txt_thumb.png'

    elsif attachment_content_type =~ /pdf|ps/
      return 'admin/files_thumbs/pdf_thumb.png'

    elsif attachment_content_type =~ /mp3|wav|aac/
      return 'admin/files_thumbs/mp3_thumb.png'
    end

    return 'admin/files_thumbs/dat_thumb.png'
  end

  def is_image?
    if attachment_content_type =~ /jpeg|jpg|png/
      return true
    else
      return false
    end
  end

  def is_pdf?
    if attachment_content_type =~ /pdf/
      return true
    else
      return false
    end
  end

  def has_custom_crop?
    return (crop_x && crop_y && crop_w && crop_h && crop_s)
  end

  def dynamic_styles
    styles = {}
    if is_image? # || is_pdf?
      styles[:small] = '50'
      if has_custom_crop?
        styles[:cropped] = { geometry: '', convert_options: "-strip -resize #{crop_s}% -crop #{crop_w}x#{crop_h}+#{crop_x}+#{crop_y}" }
      else
        styles[:cropped] = { geometry: '1280x1280>', convert_options: '-strip -quality 85', source_file_options: '-density 72' }
      end
    end
    return styles
  end

  def attachment_url(style = nil)
    # defaults to cropped style if that style exists, otherwise use original
    style ||= is_image? ? 'cropped' : 'original'
    return attachment.url(style)
  end

  def is_protected
    ActiveSupport::Deprecation.warn(
      'SpudMedia#is_protected is deprecated and will soon be removed',
      caller
    )
    false
  end

end
