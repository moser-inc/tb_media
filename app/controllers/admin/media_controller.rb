class Admin::MediaController < Admin::ApplicationController
  layout 'admin/detail'
  add_breadcrumb 'Media', :admin_media_path
  belongs_to_app :media
  before_action :load_media, only: [:edit, :update, :show, :destroy, :set_private]

  def index
    @media = SpudMedia.order('created_at DESC').paginate(page: params[:page])
    respond_with @media
  end

  def new
    @page_name = 'New Media'
    add_breadcrumb 'New', :new_admin_medium_path
    @media = SpudMedia.new
    respond_with @media
  end

  def create
    @page_name = 'New Media'
    add_breadcrumb 'New', :new_admin_medium_path
    @media = SpudMedia.new(media_params)
    location = admin_media_path
    if @media.save
      flash[:notice] = 'File uploaded successfully'
      location = edit_admin_medium_path(@media.id) if @media.is_image?
    end
    respond_with @media, location: location
  end

  def edit
    unless @media.is_image?
      flash[:error] = "Unable to edit #{@media.attachment_file_name}"
      redirect_to admin_media_path
    end
  end

  def update
    @media.attachment.reprocess! if @media.update(media_params)
    respond_with @media, location: admin_media_path
  end

  def destroy
    flash[:notice] = 'File successfully destroyed' if @media.destroy
    respond_with @media, location: admin_media_path
  end

  private

  def load_media
    @media = SpudMedia.where(id: params[:id]).first
    if @media.blank?
      flash[:error] = 'Media Asset not found!'
      redirect_to(admin_media_path()) && return
    end
  end

  def media_params
    params.require(:spud_media).permit(
      :attachment_content_type, :attachment_file_name,
      :attachment_file_size, :attachment,
      :crop_x, :crop_y, :crop_w, :crop_h, :crop_s)
  end

end
