class Admin::MediaPickerController < Admin::ApplicationController

  layout false
  respond_to :html

  def index
    @media = SpudMedia.order('attachment_file_name asc').paginate(page: params[:page], per_page: 30)
    respond_with @media do |format|
      format.html do
        if params[:only_list]
          render partial: 'media', collection: @media
        else
          render 'index'
        end
      end
    end
  end

  def create
    @media = SpudMedia.new(media_params)
    if @media.save
      render 'create', status: :ok
    else
      render text: @media.errors.full_messages.first, status: :unprocessable_entity
    end
  end

  private

  def media_params
    params.require(:spud_media).permit(:attachment_content_type, :attachment_file_name, :attachment_file_size, :attachment, :crop_x, :crop_y, :crop_w, :crop_h, :crop_s)
  end

end
