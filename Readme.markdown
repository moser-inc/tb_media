# TB Media

TB Media is an engine for managing documents and other miscellaneous media files, designed for use with [Twice Baked][1].

## Installation/Usage

1. In your Gemfile add the following

    gem 'tb_media'

2. Run bundle install
3. Copy in database migrations to your new rails project

        bundle exec rake railties:install:migrations
        rake db:migrate

4. Run a rails server instance and point your browser to `/admin`

## Configuration

TB Media accepts the following configuration options:

    Spud::Media.configure do |config|

        # s3 storage requires the 'aws-sdk' gem. defaults to filesystem
        config.paperclip_storage = :s3
        config.s3_credentials = "#{Rails.root}/config/s3.yml"

        config.storage_path = "public/system/spud_media/:id/:style/:basename.:extension"
        config.storage_url = "/system/spud_media/:id/:style/:basename.:extension"
        config.max_upload_size = 15.megabytes
    end

### Filesystem

Files are stored under `/public/system/spud_media` and are accessed directly by the web server. No further configuration is required, though you may customize the storage location if desired using `config.storage_path`.

Testing
-----------------

TB uses RSpec for testing. Get the tests running with a few short commands:

1. Create and migrate the databases:

    	rake db:create
		rake db:migrate

2. Load the schema in to the test database:

		rake app:db:test:prepare

3. Run the tests with RSpec

		rspec spec

After the tests have completed the current code coverage stats is available by opening ```/coverage/index.html``` in a browser.

[1]:https://bitbucket.org/westlakedesign/tb_core
