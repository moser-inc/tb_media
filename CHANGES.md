# Change Log

## v1.2.1

- Media picker now allows for setting of title and alt attributes
- Media picker also will now replace/modify the currently selected image

## v1.2.0

- Compatability with Rails 4.2, TB Core 1.3, and Bootstrap 3.

## v1.0.6

- Updates picker tool for Tinymce 4.x compatability

## v1.0.5

- Improve picker tool performance

## v1.0.4

- Fix problem with the latest version of paperclip

## v1.0.3

- Rails 4 and tb_core 1.2 compatibility

## v1.0.1

- Many bug fixes in Admin tools
- Started adding Rspec tests
- Update to new new /admin namespace

## v1.0.0

- First release as tb_media
