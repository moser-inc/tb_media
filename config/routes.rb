Rails.application.routes.draw do
  namespace :admin do
    resources :media

    # picker plugin not currently used
    # resources :media_picker, :only => [:index, :create]
  end
end
