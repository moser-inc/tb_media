describe("spud.admin.media.application", function() {
	var application;

	beforeEach(function() {
		application = spud.admin.media;
	});
	afterEach(function() {
		application = null;
	});

	it ("should parse valid integers as integers", function() {
		expect(application.parseIntOrDefault({val: 1})).toBe(1);
		expect(application.parseIntOrDefault({val: "0"})).toBe(0);
		expect(application.parseIntOrDefault({val: "1"})).toEqual(1);
	});
	it ("should return default value for invalid integers", function() {
		expect(application.parseIntOrDefault({val: "d1", defVal:"auto"})).toEqual("auto");
	});
	it ("should return 0 for invalid integers if not default value is specified", function() {
		expect(application.parseIntOrDefault({val: "d1"})).toEqual(0);
	});
	
});