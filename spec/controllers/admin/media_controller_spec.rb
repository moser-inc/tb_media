require 'rails_helper'

describe Admin::MediaController, type: :controller do

  before :each do
    activate_authlogic
    u = SpudUser.new(login: 'testuser', email: 'test@testuser.com', password: 'test', password_confirmation: 'test')
    u.super_admin = true
    u.save
    @user = SpudUserSession.create(u)
  end

  describe 'index' do
    it 'index page should return all media in descending order by create_at' do
      @media1 = FactoryBot.create(:spud_media, created_at: 1.hour.ago)
      @media2 = FactoryBot.create(:spud_media, created_at: 2.hours.ago)
      @media3 = FactoryBot.create(:spud_media, created_at: 3.hours.ago)
      get :index
      @medias = assigns(:media)
      expect(@medias.count).to eq(3)
      expect(@medias[0].id).to eq(@media1.id)
      expect(@medias[1].id).to eq(@media2.id)
      expect(@medias[2].id).to eq(@media3.id)
    end
  end

  describe 'new' do
    it 'should return a new media object' do
      get :new
      @media = assigns(:media)
      expect(@media).to_not be_blank
    end
  end

  describe 'create' do
    it "should create a new media and returned the saved object's edit page" do
      expect do
        post :create, params: { spud_media: FactoryBot.attributes_for(:spud_media) }
      end.to change(SpudMedia, :count).by(1)
    end
  end

end
