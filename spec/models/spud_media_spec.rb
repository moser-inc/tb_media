require 'rails_helper'

describe SpudMedia, type: :model do

  describe 'attachment' do
    it 'should require an attachment' do
      media = FactoryBot.build(:spud_media, attachment: nil)
      expect(media.valid?).to eq(false)
      expect(media.errors[:attachment].length).to eq(1)
    end
  end

end
