module Spud
  module Media
    include ActiveSupport::Configurable
    require 'active_support/core_ext/numeric/bytes'
    config_accessor :paperclip_storage, :s3_credentials, :storage_path, :storage_url, :max_upload_size
    self.paperclip_storage = :filesystem
    self.s3_credentials = "#{Rails.root}/config/s3.yml"
    self.storage_path = ':rails_root/public/system/spud_media/:id/:style/:basename.:extension'
    self.storage_url = '/system/spud_media/:id/:style/:basename.:extension'
    self.max_upload_size = 15.megabytes
  end
end
