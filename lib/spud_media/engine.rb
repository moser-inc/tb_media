require 'tb_core'
require 'paperclip'

module Spud
  module Media
    class Engine < Rails::Engine
      engine_name :tb_media

      initializer :admin do
        config.assets.precompile += ['admin/files_thumbs/*']
        config.assets.precompile += ['admin/media_thumb.png']
        # config.assets.precompile += ['admin/media/picker.js'] <- Picker not currently used
        TbCore.append_admin_javascripts('admin/media/application')
        TbCore.append_admin_stylesheets('admin/media/application')
        TbCore.configure do |config|
          config.admin_applications += [{ name: 'Media', thumbnail: 'admin/media_thumb.png', url: '/admin/media', order: 3, retina: true }]
        end
      end

      initializer 'tb_media.assets' do
        Rails.application.config.assets.precompile += ['admin/media_thumb.png']
      end

    end
  end
end
