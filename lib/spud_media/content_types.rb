module Spud
  module Media
    CONTENT_TYPES = [
      # Microsoft
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'application/msword',
      'application/vnd.ms-excel',
      'application/vnd.ms-powerpoint',

      # Documents
      'application/pdf',
      'text/html',
      'text/plain',
      'text/rtf',
      'application/xml',

      # Archives
      'application/x-compressed',
      'application/x-tar',
      'application/x-gtar',
      'application/x-gzip',
      'application/zip',
      'application/gzip',
      'application/zip',

      # Audio
      'audio/mp4',
      'audio/mpeg',
      'audio/ogg',
      'audio/vorbis',
      'audio/vnd.wave',
      'audio/webm',
      'audio/x-aiff',
      'audio/x-wav',

      # Images
      'image/bmp',
      'image/gif',
      'image/jpeg',
      'image/pjpeg',
      'image/png',
      'image/svg+xml',
      'image/tiff',

      # Video
      'video/avi',
      'video/mpeg',
      'video/mp4',
      'video/ogg',
      'video/quicktime',
      'video/webm',
      'video/x-flv',

      # Adobe
      'image/vnd.adobe.photoshop',
      'application/postscript',
      'application/x-shockwave-flash'
    ].freeze
  end
end
