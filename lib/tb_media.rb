module Spud
  module Media
    require 'spud_media/configuration'
    require 'spud_media/content_types'
    require 'spud_media/engine' if defined?(Rails)
  end
end
