FactoryBot.define do

  factory :spud_media do
    attachment Rack::Test::UploadedFile.new('spec/fixtures/images/test_img1.png', 'image/png')
  end
end
